import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TableMultiplicationComponent } from './table-multiplication/table-multiplication.component';
import { TablesMultiplicationsComponent } from './tables-multiplications/tables-multiplications.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TableMultiplicationComponent,
    TablesMultiplicationsComponent,
    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
title = 'Multiplication';


num1: 0;
num2: 0;

tableForm!: FormGroup;

isSubmitted = false;
badNumber = false;

ngOnInit(): void
  {
    this.tableForm = new FormGroup
    (
    {
    num1: new FormControl(''),
    num2: new FormControl('')
    }
    );
  }

  get formControls()
  {
    return this.tableForm.controls;
  }

   submit1(): number{
     this.isSubmitted = true;
     if (this.tableForm.value.num1 === '')
     {
      this.badNumber = true;
      return;
     }else{
       this.badNumber = false;
     }
     this.num1 = this.tableForm.value.num1;
     console.log('num :' + this.tableForm.value.num1);
   }


  submit2(): number{
    this.isSubmitted = true;
    if (this.tableForm.value.num2 === '')
    {
      this.badNumber = true;
      return;
    }else{
      this.badNumber = false;
    }
    this.num2 = this.tableForm.value.num2;
    console.log('num :' + this.tableForm.value.num2);
  }


  }




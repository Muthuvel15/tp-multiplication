import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-tables-multiplications',
  templateUrl: './tables-multiplications.component.html',
  styleUrls: ['./tables-multiplications.component.scss']
})
export class TablesMultiplicationsComponent implements OnInit {
  tab = [ {num:1},{num:2},{num:3},{num:4},{num:5},{num:6},{num:7},{num:8},{num:9},{num:10} ];
  tab2 = [ {num:1},{num:2},{num:3},{num:4},{num:5},{num:6},{num:7},{num:8},{num:9},{num:10} ];

  @Input() allTable!: number;
  @Input() tableNum!: number;

  isSubmitted = false;


  tableForm!: FormGroup;

  constructor() { }
  ngOnInit(): void
  {

  }

  addValeur(): number{
    for(let i = this.afficheNombre(); i <= this.tab.length; i++){
      return i;
    }
  }

  afficheNombre(): number
  {
    console.log(this.allTable);
    return this.allTable;
  }


}

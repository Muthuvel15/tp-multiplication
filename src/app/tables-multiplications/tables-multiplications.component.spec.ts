import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesMultiplicationsComponent } from './tables-multiplications.component';

describe('TablesMultiplicationsComponent', () => {
  let component: TablesMultiplicationsComponent;
  let fixture: ComponentFixture<TablesMultiplicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablesMultiplicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesMultiplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

= TP : Table de multiplication
Doc Writer <doc.writer@asciidoc.org>
v1.0, 2012-01-01

== Présentation


Mission de développement d’une application web, qui affiche une table multiplication.

---
#### Information gérénal
---
[horizontal]
Réalise par:: SAVOUNDIRAPANDIANE Muthuvel
Langugue de informatique::  TypeScript/Javascript
IDE:: Visual Studio Code
Framework::  Angular /Modéle MVC

---
## Premier Partie

J'ai réalisé une formulaire qui prend un nombre pour obtenir une table de multiplication.

Voici le formulaire:

image::src/assets/Images/image-2020-11-12-21-22-30-537.png[]


Le code formulaire est situé arborescence *app répertoire

image::src/assets/Images/image-2020-11-12-21-29-37-650.png[]

Le code du formulaire app.components.html
----
<section class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
        <h3 class="title is-3">Affiche une table </h3>
        <form [formGroup]="tableForm" (ngSubmit)="submit1()">
          <div class="field">
              <div class="control">
                <label>Enter un nombre</label>
                <p [ngClass]="{ 'has-error': isSubmitted && badNumber}">
                  <input class="input is-success" formControlName="num1" type="number" placeholder="">
                </p>
                <div *ngIf="isSubmitted && badNumber" class="help-block">
                  <div *ngIf="badNumber">Un nombre est demandé !</div>
                  </div>
              </div>
          </div>
          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Generer le tableau</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<div *ngIf=num1>
  <app-table-multiplication [tableNum]="num1" ></app-table-multiplication>
</div>

----
UML du TP Multiplication

image::src/assets/Images/image-2020-11-12-22-42-51-933.png[]

L'utilisateur saisir une valeur dans la formulaire.

-----
<form [formGroup]="tableForm" (ngSubmit)="submit1()">
          <div class="field">
              <div class="control">
                <label>Enter un nombre</label>
                <p [ngClass]="{ 'has-error': isSubmitted && badNumber}">
                  <input class="input is-success" formControlName="num1" type="number" placeholder="">
                </p>
                <div *ngIf="isSubmitted && badNumber" class="help-block">
                  <div *ngIf="badNumber">Un nombre est demandé !</div>
                  </div>
              </div>
          </div>
-----
Dans la balise <forme>,formcontrolname est stockée dans une variable num1. App .components.ts récupère la valeur (num1) qui saisit dans le formulaire. Ainsi on fait la liaison entre le formulaire HTML et la propriété du code behind.
-----

tableForm!: FormGroup;

isSubmitted = false;
badNumber = false;

ngOnInit(): void
  {
    this.tableForm = new FormGroup
    (
    {
    num1: new FormControl(''),
    num2: new FormControl('')
    }
    );
  }
-----
*ngOnInit* est un hook de cycle de vie appelé par Angular pour indiquer que Angular a terminé la création du composant.
Dans cette méthode on inscialise les valeurs transmissent par *app Component HTML*
-----
submit1(): number{
this.isSubmitted = true;
if (this.tableForm.value.num1 === '')
{
this.badNumber = true;
return;
}else{
this.badNumber = false;
}
this.num1 = this.tableForm.value.num1;
console.log('num :' + this.tableForm.value.num1);
}
-----
La méthode submit() récupérer la valeur transmie par l'utilisateur et vérifier qu'il a bien saisi la valeur.
-----
import { Component ,Input,OnInit } from '@angular/core';
-----

la réception de données du composant parent à l’enfant, ce sera le rôle du « décorateur » @Input

----
export class TableMultiplicationComponent implements OnInit
{
  tab = [ {num:1},{num:2},{num:3},{num:4},{num:5},{num:6},{num:7},{num:8},{num:9},{num:10} ];

  @Input() tableNum!: number;

----

La variable du num1 est transmie au tableNum dans la partie HTML. L'attribute tablenum est visible au `app.component.ts` et qu'il a un rôle parent.
----
<div *ngIf=num1>
  <app-table-multiplication [tableNum]="num1" ></app-table-multiplication>
</div>
----
Dans la partie table-Multiplication nous avons des fichiers `table-Multplication.components.ts` et `table-Multplication.components.html`.
Sur le composant table-Multplication.components.ts on a une méthode `afficheNombre()` qui retourne la valeur tableNum.
----
afficheNombre()
{
console.log(this.tableNum);
return this.tableNum;
}

----
Dans le fichier table-Multplication.components.html, j'ai fait une boucle pour créer un liste et puis multiplie avec la variable tableNum.

-----
<div class="container is-fluid" >
    <div class="notification is-primary">
      La table de {{afficheNombre()}}
      <ul>
          <li style *ngFor="let numbers of tab">{{tableNum}} X {{numbers.num}} = {{numbers.num * tableNum}} </li>
        </ul>
    </div>
</div>
-----
Voici le résultat :

image::src/assets/Images/image-2020-11-13-01-09-00-375.png[]





## Deuxième Partie

J'ai un crée un autre composant s'appelle tables-multiplications. La différence est que le composant table-multiplication générer une seule table multiplication alors que la tables-multplication générer entre 1 à 10 table de multiplication.

Voici le code tables-multiplication.component.ts

----

export class TablesMultiplicationsComponent implements OnInit {
  tab = [ {num:1},{num:2},{num:3},{num:4},{num:5},{num:6},{num:7},{num:8},{num:9},{num:10} ];
  tab2 = [ {num:1},{num:2},{num:3},{num:4},{num:5},{num:6},{num:7},{num:8},{num:9},{num:10} ];

  @Input() allTable!: number;
  @Input() tableNum!: number;

  isSubmitted = false;


  tableForm!: FormGroup;

  constructor() { }
  ngOnInit(): void
  {

  }

----
Dans le décorateur @input on a une attribut allTable qui récuperer la valeur num2 dans le formulaire de app composante.
----
<div *ngIf=num2>
<app-tables-multiplications [allTable]="num2"></app-tables-multiplications>
<div>
----
Enfin j'ai réaliser une boucle pour afficher des colonnes qui est situées dans le tables-multiplications.componemts.html
----
<div class="row card-deck" *ngFor="let numbers of tab2">
  <div class="col-lg-3 col-md-6" >
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">La table de {{addValeur()}}</h5>
          <ol>
            <li style *ngFor="let numbers of tab">{{allTable}} X {{numbers.num}} = {{numbers.num * allTable}} </li>
          </ol>
        </div>
      </div>
    <br/>
</div>
</div>
----
Dans la basile h5 j'ai mis la méthode addValeur en utilisant la méthode string interporation pour affiche la valeur donne par l'utilisateur.

---
Voici le résultat:

image::src/assets/Images/image-2020-11-13-01-22-10-907.png[]

---
